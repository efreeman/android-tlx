package uk.co.euanfreeman.tlx.util;

public class StorageUnavailableException extends Exception {
	private static final long serialVersionUID = -4433582202183852218L;
	
	public StorageUnavailableException(String message) {
		super(message);
	}
}
