package uk.co.euanfreeman.tlx;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This activity uses a webview to run a NASA TLX questionnaire.
 * 
 * @author Euan Freeman, Mark McGill
 */
public class TLXActivity extends Activity {
	private WebView mWebView;
	private TLXWebApp mWebApp;
	private EditText mParticipantText;
	private EditText mBlockText;
	private LinearLayout mContainer;
	private TextView mMessage;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_tlx);
		
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.getSettings().setJavaScriptEnabled(true);
		
		mParticipantText = (EditText) findViewById(R.id.participant_number);
		mBlockText = (EditText) findViewById(R.id.block_number);
		mContainer = (LinearLayout) findViewById(R.id.container);
		mMessage = (TextView) findViewById(R.id.message);
	}
	
	/**
	 * Button callback which starts the TLX questionnaire.
	 */
	public void startTLX(View trigger) {
		String pId = mParticipantText.getText().toString();
		String bId = mBlockText.getText().toString();
		
		if (pId.length() == 0 || bId.length() == 0) {
			Toast.makeText(this, "Please enter participant ID and block number", Toast.LENGTH_SHORT).show();
			
			return;
		}
		
		String descriptor = String.format("-TLX%s", bId);
		
		mWebApp = new TLXWebApp(this, Integer.parseInt(pId), descriptor);
		
		mWebView.addJavascriptInterface(mWebApp, "Android");
		mWebView.loadUrl("file:///android_asset/tlx.html");
		
		mContainer.setVisibility(View.INVISIBLE);
		mMessage.setVisibility(View.INVISIBLE);
		mWebView.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Hide the TLX questionnaire and show a message to the participant.
	 */
	public void finishedTLX() {
		runOnUiThread(actionFinishTLX);
	}
	
	/**
	 * This runnable should be run on the UI thread. Hides the
	 * TLX webview and displays a message to the participant.
	 */
	private Runnable actionFinishTLX = new Runnable() {
		@Override
		public void run() {
			mWebView.setVisibility(View.INVISIBLE);
			
			mMessage.setText("End of questionnaire - please tell the experimenter.");
			mMessage.setVisibility(View.VISIBLE);
		}
	};
}
