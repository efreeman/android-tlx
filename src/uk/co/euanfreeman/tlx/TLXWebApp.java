package uk.co.euanfreeman.tlx;

import java.util.Arrays;

import uk.co.euanfreeman.tlx.util.ParticipantDataLog;

import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Javascript interface for the TLX web app.
 * 
 * @author Euan Freeman, Mark McGill
 */
public class TLXWebApp {
	private TLXActivity mParent;
	private ParticipantDataLog mLog;
	
	public TLXWebApp(TLXActivity activity, int participantId, String fileDescriptor) {
		mParent = activity;
		
		mLog = new ParticipantDataLog(participantId, fileDescriptor);
	}
	
	@JavascriptInterface
    public void alert(String message) {
        Toast.makeText(mParent, message, Toast.LENGTH_SHORT).show();
    }
    
    @JavascriptInterface
    public void logSurveyResults(int[] results){
        String resultString = Arrays.toString(results);
        resultString = resultString.substring(1, resultString.length()-1);
        
        mLog.write(resultString);
        
        mParent.finishedTLX();
    }
}
